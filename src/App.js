import data from "./data/lexicon.json"

import "../node_modules/bulma/css/bulma.css"
import { useEffect, useState } from "react";

function App() {

  const [showData,setShowData] = useState()
  const [showResponse,setShowResponse] = useState(false)

  useEffect(()=>{
    setShowData(data[Math.floor(Math.random() * data.length)])
  },[])

  const change =()=>{
    setShowData(data[Math.floor(Math.random() * data.length)])
    setShowResponse(false)
  }

  return (
    <div>
      <h1 className='title has-text-centered'>Lexicode</h1>
      <div className="columns">
        <div className="column is-3 is-offset-1 is-offset-1-mobile">
          {showData?.name}
        </div>
        <div className="column is-8 is-offset-1-mobile">
          {showResponse && showData?.description}
        </div>
      </div>
      <div className="columns">
      <div className="column is-3 is-offset-1 is-offset-1-mobile">
          <button className="button" onClick={e=>setShowResponse(true)}>Réponse</button>
        </div>
        <div className="column is-8 is-offset-1-mobile">
          <button  className="button" onClick={e=>change()}>Change</button>
        </div>
      </div>
    </div>
  );
}

export default App;
